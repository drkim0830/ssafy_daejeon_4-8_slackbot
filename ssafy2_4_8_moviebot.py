# -*- coding: utf-8 -*-
import json
import re
import requests
import urllib.request
import urllib.parse

from bs4 import BeautifulSoup
from flask import Flask, request
from slack import WebClient
from slack.web.classes import extract_json
from slack.web.classes.blocks import *
from slack.web.classes.elements import *
from slack.web.classes.interactions import MessageInteractiveEvent
from slackeventsapi import SlackEventAdapter


SLACK_TOKEN = ''
SLACK_SIGNING_SECRET = ''


app = Flask(__name__)
# /listening 으로 슬랙 이벤트를 수신
slack_events_adaptor = SlackEventAdapter(SLACK_SIGNING_SECRET, "/listening", app)
slack_web_client = WebClient(token=SLACK_TOKEN)

# keyword로 받은 영화가 현재 상영중인 영화인지 판별
def _is_there_movie(keyword):
    url = "https://movie.naver.com/movie/bi/ti/running.nhn?code=226"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")
    movie_info = []
    href_string = ''
    title = ''
    for data in soup.find('tbody').find_all('tr'):
        href_string = data.find('a')['href']
        title = data.find('a').get_text()
        movie_info.append((title,href_string))
    
    pass_url = ''

    for i in movie_info:
        if keyword.split()[1] in i[0]:
            text_href = i[1]
            pass_url = "https://movie.naver.com" + text_href
            break

    if pass_url == '':
        return 'error'

    return pass_url


#현재상영작 or 영화명을 검색 시 크롤링 함수 구현
def _crawl_chart(keyword):

    message = ""
    if '현재상영작' in keyword:
        #롯데시네마 대전둔산(월평점) 상영시간 정보 url
        url = "https://movie.naver.com/movie/bi/ti/running.nhn?code=226"
        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")
        
        movie_info = []
        href_string = ''
        movie_title = ''
        i = 1
        #crawling - movie title, href link
        for data in soup.find('tbody').find_all('tr'):
            href_string = data.find('a')['href']
            movie_title = data.find('a').get_text()
            message += ('*`{i}`* {movie_title} \n'.format(i = i, movie_title = movie_title))
            movie_info.append((movie_title,href_string))
            i += 1
        
        head_block = SectionBlock(
            text = '*롯데시네마 대전둔산점(월평점)의 현재 상영작입니다.*'
        )

        item_fields = []
        item_fields.append(message)
        link_section = SectionBlock(fields=item_fields)
        
        advice_block = SectionBlock(
            text = '\n\n `@[봇이름] <영화명>` 으로 검색하세요!'
        )

        return [head_block, link_section, advice_block]

    else:
        #is_there_movie() 반환 값이 error일 경우 출력 (상영중인 영화가 아닐 때)
        error_block = SectionBlock(
            text = '*죄송합니다. 저희 영화관에서는 현재 상영중인 영화가 아닙니다!*' + '\n `@[봇이름] "현재상영작"` 을 검색해보세요!'
        )
        #상영중인지 판별, 있으면 해당 영화의 href link를 반환
        url = _is_there_movie(keyword)
        if url == 'error':
            return [error_block]

        source_code = urllib.request.urlopen(url).read()
        soup = BeautifulSoup(source_code, "html.parser")
        
        #영화 정보 크롤링
        items = {}
        for item_div in soup.find_all('div', class_='mv_info_area'):
            items = {
                "movie_title": item_div.find('h3', class_='h_movie').get_text()[:-3].strip(),
                "movie_img": item_div.find('div', class_='poster').find('img')['src'],
                "movie_rate": item_div.find('span',class_='st_off').get_text(),
                "movie_genre": item_div.find('dl',class_='info_spec').find_all('dd')[0].find('span').get_text().replace("\r",'').replace("\t",'').replace("\n",'').strip(),
                "movie_di": item_div.find('dl',class_='info_spec').find_all('dd')[1].get_text(),
                "movie_act": item_div.find('dl',class_='info_spec').find_all('dd')[2].get_text().replace('더보기', ''),
                "movie_class": item_div.find('dl',class_='info_spec').find_all('dd')[3].find('a').get_text(),
                "movie_run": item_div.find('dl',class_='info_spec').find_all('dd')[0].find_all('span')[2].get_text()
            }
        
        #하나의 문자열로 저장
        message =  ('*`제목`* {movie_title} \n *`평점`* {movie_rate} \n *`감독`* {movie_di} \n *`출연진`* {movie_act} \n *`러닝타임`* {movie_run} \n *`장르`* {movie_genre} \n *`등급`* {movie_class} \n'.format(movie_title = items['movie_title'], movie_rate = items['movie_rate'], movie_di = items['movie_di'], movie_act = items['movie_act'], movie_run = items['movie_run'], movie_genre = items['movie_genre'], movie_class = items['movie_class']))
        # return message

        #영화 포스터    
        first_item_image = ImageElement(
            image_url=items["movie_img"],
            alt_text= keyword + "이미지를 불러올 수 없습니다."
        )
        #영화 정보
        item_fields = []
        item_fields.append(message)
        link_section = SectionBlock(fields=item_fields)

        # 예매, 리뷰보기 버튼
        button_actions = ActionsBlock(
            block_id=keyword,
            elements=[
                ButtonElement(
                    text="예매",  style="danger",
                    action_id="but_1", value=str(1)
                ),
                ButtonElement(
                    text="리뷰보기", style="primary",
                    action_id="but_2", value=str(2)
                ),
            ]
        )
        # 각 섹션을 list로 묶어 전달
        return [first_item_image, link_section, button_actions]

#영화의 상영시간표 및 예매 링크 출력 함수
def _timetable_chart(keyword):
    #영화 시간표 링크
    url = "https://movie.naver.com/movie/bi/ti/running.nhn?code=226"
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    time = []
    movie_title = ''
    #button 이벤트로 전달받은 입력한 keyword와 동일한 제목을 가진 tr tag 크롤링을 통해 찾아 정확한 영화명과 시간표에 보기 좋게 저장
    for i in soup.find('tbody').find_all('tr'):
        if keyword.split()[1] in i.find('a').get_text():
            movie_title = i.find('a').get_text()
            time.append(i.find("td").get_text().replace('\r','').replace('\n','').replace('\t','').replace(' ','').replace('|', ' | '))
            break
    
    #영화 시간표와 롯데시네마 상영시간표 링크(= 예매 링크)를 출력
    timetable_block = SectionBlock(
        text = ('*{keyword}의 현재 상영시간표 입니다.*'.format(keyword = movie_title)) + '\n'+'<http://www.lottecinema.co.kr/LCHS/Contents/Cinema/Cinema-Detail.aspx?divisionCode=1&detailDivisionCode=3&cinemaID=4006|_예매 링크_>'+'\n'
    )
    link_section = SectionBlock(fields=time)
    
    return [timetable_block, link_section]

#영화의 리뷰 데이터 출력 함수
def _review_chart(keyword):
    #해당 영화의 링크로 가기 위하여 href 반환
    url = _is_there_movie(keyword)
    source_code = urllib.request.urlopen(url).read()
    soup = BeautifulSoup(source_code, "html.parser")

    score_reples = []
    score = ''
    reple = ''
    star_score = ''
    #해당 영화의 평가 정보를 크롤링하여 score와 reple 값을 저장
    for data in soup.find('div', class_='score_result').find('ul').find_all('li'):
        score = data.find('div', class_='star_score').get_text().strip()
        reple = data.find('div', class_='score_reple').get_text().strip().split('\n')[0]
        star = int(score)
        star_score = ''
        
        #10점 만점의 평가를 별점으로 나타내기 위하여 별점 계산 후 출력 form에 추가
        star /= 2
        while star > 0:    
            if star >= 1:
                star_score += "★"
                star -= 1
            if star == 0.5:
                star_score += '☆'
                star -= 1
        #출력 form
        score_reple = ('*`{star_score}({score}점)`* : {reple}'.format(star_score = star_score, score = score, reple = reple))
        score_reples.append(score_reple)
    
    #list로 구성되어 있는 각각의 평점리뷰 값을 줄바꿈으로 하나의 문자열로 join
    message = ''
    message = u'\n'.join(score_reples)
    
    review_block = SectionBlock(
        text = message
    )

    return [review_block]
    

# 챗봇이 멘션을 받았을 경우
@slack_events_adaptor.on("app_mention")
def app_mentioned(event_data):
    channel = event_data["event"]["channel"]
    text = event_data["event"]["text"]
    
    if (text[0] != "<"):
        return
    message_blocks = _crawl_chart(text)

    slack_web_client.chat_postMessage(
        channel=channel,
        blocks=extract_json(message_blocks)
    )

@app.route("/click", methods=["GET", "POST"])
def on_button_click():
    # 버튼 클릭은 SlackEventsApi에서 처리해주지 않으므로 직접 처리
    payload = request.values["payload"]
    click_event = MessageInteractiveEvent(json.loads(payload))

    keyword = click_event.block_id
    flag = int(click_event.value) 

    # 버튼에 따른 다른 함수 호출
    if flag == 1:       #예매 버튼 클릭 시
        message_blocks = _timetable_chart(keyword)
    elif flag == 2:     #리뷰보기 버튼 클릭 시
        message_blocks = _review_chart(keyword)

    # 메시지를 채널에 전달
    slack_web_client.chat_postMessage(
        channel=click_event.channel.id,
        blocks=extract_json(message_blocks)
    )

    # Slack에게 클릭 이벤트를 확인(서버에 출력)
    return "OK", 200


# / 로 접속하면 서버가 준비되었다고 알림
@app.route("/", methods=["GET"])
def index():
    return "<h1>Server is ready.</h1>"


if __name__ == '__main__':
    app.run('0.0.0.0', port=8080)